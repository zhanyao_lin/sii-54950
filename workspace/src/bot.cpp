#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctime>
#include <ctype.h>
#ifndef Datos
#include <DatosMemCompartida.h>
#endif

using namespace std;
int main()
{
	DatosMemCompartida* ptrdatos=NULL;
	int fd_mem;
	struct stat bstat;

	if ((fd_mem=open("Datos", O_RDWR))<0) {
		perror("No puede abrirse el fichero origen");
	}
	if ((fstat(fd_mem, &bstat))<0) {
		perror("Error en fstat del fichero origen");
		close(fd_mem);
	}
	ptrdatos=(DatosMemCompartida*)mmap(0,bstat.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd_mem,0);
	if (ptrdatos==(DatosMemCompartida*)MAP_FAILED){
		perror("Error en la proyeccion del fichero origen");
		close(fd_mem);
	}
	close(fd_mem);
        while(1){
        float esfera_x=ptrdatos->esfera.centro.x;
        float esfera_y=ptrdatos->esfera.centro.y;
        float esfera_r=ptrdatos->esfera.radio;
        float raqueta1_y1=ptrdatos->raqueta1.y1;    
        float raqueta1_y2=ptrdatos->raqueta1.y2; 
           
        usleep(25000);
        if((esfera_y)>raqueta1_y1) ptrdatos->accion=1;
        else if (esfera_y<raqueta1_y2) ptrdatos->accion=-1;
        else ptrdatos->accion=0;
        }
	/* Se eliminan las proyecciones */
	munmap(ptrdatos, bstat.st_size);

	return 0;
}
