# Tenis #

El juego es multijugador de 2 jugadores.

El jugador 1 mueve la raqueta con las teclas "w" para subir y "s" para bajar a golpear la pelota.

El jugador 2 mueve la raqueta con las teclas "o" para subir y "l" para bajar a golpear la pelota.

La pelota se disminuye de tamaño según el tiempo trancurrido.

El ganador es el primero en alcanzar 3 puntos.

El jugador 1 puede ser un bot.

El servidor establece conexión con el cliente


