// Mundo.cpp: implementation of the CMundo class.
// Autor : ZHANYAO LIN
//////////////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctime>
#include <ctype.h>
#include <DatosMemCompartida.h>

using namespace std;
struct stat bstat;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
	estado=juego;
}

CMundo::~CMundo()
{
     close(fd_sc);
     unlink("FIFO_SC");
     close(fd_cs);
     unlink("FIFO_CS");
     munmap(ptrdatos,bstat.st_size);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	if(estado==fin){
	char mfin[100];
	if(puntos1>puntos2) sprintf(mfin,"Game over: Ha ganado el jugador1");
	else if(puntos2>puntos1) sprintf(mfin,"Game over: Ha ganado el jugador2");
	print(mfin,300,0,1,1,1);
	}
	else if(estado==juego){
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
}
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
     
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.disminuye(0.0025f);
	
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
	}
        
	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
	}
	ptrdatos->esfera=esfera;
	ptrdatos->raqueta1=jugador1;
	
	if(ptrdatos->accion==1)
	OnKeyboardDown('w',0,0);
	else if(ptrdatos->accion==-1)
	OnKeyboardDown('s',0,0);
	
	if(puntos1>=3 || puntos2>=3)
	estado=fin;
	char cad[200];
	read(fd_sc, &cad, sizeof(cad));
        sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
}
void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}*/
	char cad[100];
	sprintf(cad,"%c",key);
	write(fd_cs,&cad,sizeof(cad));
}

void CMundo::Init()
{
	  unlink("FIFO_SC");
    /* crea el FIFO sc*/
    if (mkfifo("FIFO_SC", 0600)<0) {
        perror("No puede crearse el FIFO");
        return;
    }
    /* Abre el FIFO sc*/
    if ((fd_sc=open("FIFO_SC", O_RDONLY))<0) {
        perror("No puede abrirse el FIFO");
        return;
    }
    unlink("FIFO_CS");
    /* crea el FIFO cs*/
    if (mkfifo("FIFO_CS", 0600)<0) {
        perror("No puede crearse el FIFO");
        return;
    }
    /* Abre el FIFO cs*/
    if ((fd_cs=open("FIFO_CS", O_WRONLY))<0) {
        perror("No puede abrirse el FIFO");
        return;
    }
		/*mamp*/
        int fd_mem;
	if ((fd_mem=open("Datos", O_RDWR|O_CREAT|O_TRUNC,0666))<0) {
		perror("No puede abrirse el fichero");
		}
		write(fd_mem,&datos,sizeof(datos));
	
	if((fstat(fd_mem,&bstat))<0){
		perror("No puede abrirse el fichero");
		close(fd_mem);
		}
		ptrdatos=(DatosMemCompartida*)mmap(0,bstat.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd_mem,0);
	if (ptrdatos==(DatosMemCompartida*)MAP_FAILED){
		perror("Error en la proyeccion del fichero origen");
		close(fd_mem);
	}
	close(fd_mem);
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
