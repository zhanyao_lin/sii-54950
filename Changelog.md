# Changelog

Este documento describe los cambios realizados durante el proyecto.

## [Prática 4]

## [4.0] - 2021-12-12
### Changed
- conexión entre cliente y servidor
### Added
- servidor
## [Prática 3]

## [3.0] - 2021-11-22
### Changed
- Fin de la partida si algún jugador alcanza 3 puntos
### Added
- logger
- bot de raqueta
## [Prática 2]

## [2.0] - 2021-10-30
### Changed
- Movimiento en pelota de tenis y raquetas.
- Disminución de tamaño en la pelota según tiempo pasado.
### Added
- Readme.md.

## [Prática 1]

## [1.1] - 2021-10-22
### Added
- Changelog.md.
- Autor en los ficheros.
